#!/usr/bin/env python3
# Initial commit: 7t of may 2020

import argparse
from datetime import datetime
import os
import xml.etree.ElementTree as ET
import shutil
import bcrypt


def addSectionHeader(title):
    return "<h2>"+title+"</h2>"


def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)


# Replace pattern from template
def writeOutput(pattern, content):
    with open('output.html', "r") as file:
        filedata = file.read()
    filedata = filedata.replace(pattern, content)
    with open('output.html', "w") as file:
        file.write(filedata)


# List of elements (not indexed)
def writeTable(allElements):
    html = ''
    for elem in allElements :                                                       
        html += '<tr>'                                                          
        for key in elem:                                                        
            html += '<td>'+str(elem[key])+'</td>'                               
        html += '</tr>' 
    return html


def writeInfo(hostInfo, pattern, header, section):
    html = "<table id='smallcells'>"
    if header != '':
        html += '<tr>'
        html += '<th colspan="2">'+header+'</th>'
        html += '</tr>'
    for key in hostInfo:
        html += '<tr>'
        html += '<td>'+str(key)+'</td>'
        html += '<td>'+str(hostInfo[key])+'</td>'
        html += '</tr>'
    html += '</table><br>'
    writeOutput(pattern, html)

    if section == "admin":
        with open("./info/admin-access.html", "r") as f:
            content = f.read()
            writeOutput('<!-- INFO - ADMIN ACCESS --!>', content)
    if section == "syslog":
        with open("./info/syslog.html", "r") as f:
            content = f.read()
            writeOutput('<!-- INFO - LOGGING --!>', content)


def writeUsers(allUsers, pattern):
    html = "<table id='smallcells'><tr>"
    html += "<th>Name</th>"
    html += "<th>Description</th>"
    html += "<th>Bcrypt hash</th>"
    html += "<th>UID</th>"
    html += "<th>Privileges</th>"
    html += "<th>Default password</th>"
    html += "</tr>"

    html += writeTable(allUsers)

    html += '</table><br>'  

    writeOutput(pattern, html)

    # Write additionnal information about users
    with open("./info/users.html", "r") as f:
        content = f.read()
        writeOutput('<!-- INFO - USERS --!>', content)

    print('   + Users: DONE !')


def writeAliases(allAliases, pattern):
    html = addSectionHeader('Aliases')
    html += "<table id='smallcells'><tr>"
    html += "<th>Name</th>"
    html += "<th>Address</th>"
    html += "<th>Description</th>"
    html += "<th>Type</th>"
    html += "</tr>"

    html += writeTable(allAliases)
    
    html += '</table><br>'
    writeOutput(pattern, html)

    # Write additionnal information about users
    with open("./info/aliases.html", "r") as f:
        content = f.read()
        content += '<br><br><hr style="text-align:left;margin-left:0">'
        writeOutput('<!-- INFO - ALIASES --!>', content)

    print('   + Aliases: DONE !')


def writeCert(allCerts, allCa, pattern):
    html = addSectionHeader('Certificates')

    html += "<table class='footable' id='certs' width='1000px' data-show-toggle='false'>"
    html += "<th>ID</th>"
    html += "<th>Description</th>"
    html += "<th data-breakpoints='all' class='test'></th>"
    html += "<th data-breakpoints='all' class='test'></th>"
    html += "</tr>"
    
    if len(allCerts) > 0:
        html += '<tr><th colspan="2">'
        html += 'Server certificates'
        html += '</th></tr>'
        html += writeTable(allCerts)
    if len(allCa) > 0:
        html += '<tr><th colspan="2">'
        html += 'CA certificates'
        html += '</th></tr>'
        html += writeTable(allCa)

    html += '</table><br>'  
    writeOutput(pattern, html)

    # Write additionnal information about users
    with open("./info/certs.html", "r") as f:
        content = f.read()
        content += '<br><br><hr style="text-align:left;margin-left:0">'
        writeOutput('<!-- INFO - CERTS --!>', content)


def writeOvpn(allOvpnServ, pattern):
    html = addSectionHeader('OpenVPN')
    html += "<table id='smallcells'>"
    html += '<tr><th colspan="10">General OpenVPN servers parameters</th></tr>'
    html += "<tr><th>ID</th>"
    html += "<th>Mode</th>"
    html += "<th>Protocol</th>"
    html += "<th>Device mode</th>"
    html += "<th>Interface</th>"
    html += "<th>Local port</th>"
    html += "<th>Local net</th>"
    html += "<th>Tunnel net</th>"
    html += "<th>Remote net</th>"
    html += "<th>Description</th>"
    html += "</tr>"

    # Main VPN server parameters
    for ovpnServ in allOvpnServ:
        html += '<tr>'
        for key in ovpnServ[0]:
            html += '<td>'+str(ovpnServ[0][key])+'</td>'
        html += '</tr>'
    
    html += '</table><br>'

    # Crypto parameters
    html += "<table id='smallcells'><tr>"
    html += '<tr><th colspan="8">Crypto parameters</th></tr>'
    html += "<tr><th>Id</th>"
    html += "<th>Compression</th>"
    html += "<th>Certref</th>"
    html += "<th>Crypto</th>"
    html += "<th>Digest</th>"
    html += "<th>TLS type</th>"
    html += "<th>NCP enabled</th>"
    html += "<th>NCP ciphers</th>"
    html += "</tr>"
    for ovpnServ in allOvpnServ:
        html += '<tr>'
        for key in ovpnServ[1]:
            html += '<td>'+str(ovpnServ[1][key])+'</td>'
        html += '</tr>'
    html += '</table><br>'

    writeOutput(pattern, html)

    # Write additionnal information about users
    with open("./info/openvpn.html", "r") as f:
        content = f.read()
        content += '<br><br><hr style="text-align:left;margin-left:0">'
        writeOutput('<!-- INFO - OpenVPN --!>', content)


def writeGw(allGw, default4, default6, gwGroup, pattern):
    html = addSectionHeader('Gateways (newer versions)')
    html += "<h3> Default gateways </h3>"
    html += "<ul>"
    html += "<li>Default gateway IPv4: "+str(default4)+'</li>'
    html += "<li>Default gateway IPv6: "+str(default6)+'</li>'
    html += "</ul>"
    html += "<h3> Defined gateways </h3>"
    html += "<table id='smallcells'><tr>"
    html += "<th>Interface</th>"
    html += "<th>Gateway</th>"
    html += "<th>Name</th>"
    html += "<th>Default</th>"
    html += "<th>Description</th>"
    html += "</tr>"
    for gw in allGw:
        html += '<tr>'
        for key in gw:
            html += '<td>'+str(gw[key])+'</td>'
        html += '</tr>'
    html += '</table><br>'
    
    if bool(gwGroup) is True:
        html += writeGwGroup(gwGroup)

    writeOutput(pattern, html)

    with open("./info/gateways.html", "r") as f:
        content = f.read()
        content += '<br><br><hr style="text-align:left;margin-left:0">'
        writeOutput('<!-- INFO - GATEWAYS --!>', content)



def writeGwGroup(gwGroup):
    html = "<h3> Gateway group </h3>"
    html += "<table id='smallcells'><tr>"
    html += "<th>Name</th>"
    html += "<th>Items</th>"
    html += "<th>Trigger</th>"
    html += "<th>Description</th>"
    html += "</tr>"
    html += '<tr>'
    for key in gwGroup:
        html += '<td>'+str(gwGroup[key])+'</td>'
    html += '</tr></table><br>'
    return html


def writeInterfaces(allInterfaces, pattern):
    html = "<table id='smallcells'><tr>"
    html += "<th>PfSense Name</th>"
    html += "<th>System Name</th>"
    html += "<th>Enabled</th>"
    html += "<th>IPv4 address</th>"
    html += "<th>IPv4 subnet</th>"
    html += "<th>IPv6 address</th>"
    html += "<th>IPv6 subnet</th>"
    html += "<th>Gateway</th>"
    html += "</tr>"
    for interface in allInterfaces:
        html += '<tr>'
        for key in interface:
            html += '<td>'+str(interface[key])+'</td>'
        html += '</tr>'
    html += '</table>'

    writeOutput(pattern, html)
    print('   + Interfaces: DONE !')


def writeRoutes(allRoutes, pattern):
    html = addSectionHeader('Static Routes')
    html += "<table id='smallcells'><tr>"
    html += "<th>Interface</th>"
    html += "<th>Network</th>"
    html += "<th>Gateway</th>"
    html += "<th>Description</th>"
    html += "</tr>"
    for route in allRoutes:
        html += '<tr>'
        for key in route:
            html += '<td>'+str(route[key])+'</td>'
        html += '</tr>'
    html += '</table><br>'

    writeOutput(pattern, html)
    with open("./info/routes.html", "r") as f:
        content = f.read()
        content += '<br><br><hr style="text-align:left;margin-left:0">'
        writeOutput('<!-- INFO - ROUTES --!>', content)

    print('   + Routes: DONE !')


def writeRules(allRules, allSeparators, pattern):
    html = addSectionHeader('Filtering - Rules ')
    html += '<div id="tabs"><ul id="horizontal-list">'
    for iface in allRules:
        html += '<li><a href="#tabs-'+iface+'">'+iface+'</a></li>'
    html += '</ul>'

    for iface in allRules:
        html += '<div id="tabs-'+iface+'"><br>'
        html += "<table id='smallcells'><tr>"
        html += "<th>Enabled</th>"
        html += "<th>Type</th>"
        html += "<th>Interface</th>"
        html += "<th>Protocol</th>"
        html += "<th>Source</th>"
        html += "<th>Destination</th>"
        html += "<th>NAT id</th>"
        html += "<th>Comment</th>"
        html += "</tr>"

        # Separators should be added per interface. They are numbered and should 
        # be inserted at the given index. 
        # For each interface, get the list of separators indexes and insert the 
        # separators when their index is reached
        ctr = 0
        for rule in allRules[iface]:
            for sep in allSeparators[iface]:
                if ctr == int(sep['Row']):
                    descr = str(sep['Description'])
                    color = str(sep['Color'])
                    html += '<tr>'
                    html += '<td colspan="8" class="'+color+'">'+descr+'</td>'
                    html += '</tr>'

            html += '<tr>'
            for item in rule:
                html += '<td>'+str(rule[item])+'</td>'
            html += '</tr>'
            ctr += 1
        html += '</table><br>'

        html += '</br></div>'
    html += '</div>'

    writeOutput(pattern, html)

    # Additionnal information 
    with open("./info/filtering.html", "r") as f:
        content = f.read()
        content += '<br><br><hr style="text-align:left;margin-left:0">'
        writeOutput('<!-- INFO - FILTERING --!>', content)
    print('   + Firewall rules: DONE !')


def writeNat(allNat, pattern, subHeader, setHeader):
    html = ''
    if setHeader == True:
        html = addSectionHeader('NAT - Rules ')
    if subHeader != '':
        html += "<h3>"+subHeader+"</h3>"
    html += "<table id='smallcells'><tr>"
    html += "<th>Interface</th>"
    html += "<th>Source</th>"
    html += "<th>Destination</th>"
    if subHeader == 'Port forwarding':
        html += "<th>Protocol</th>"
        html += "<th>Target</th>"
        html += "<th>Local Port</th>"
        html += "<th>NAT id</th>"
    if subHeader == 'One to One':
        html += "<th>External</th>"
    html += "<th>Description</th>"
    html += "</tr>"
    for nat in allNat:
        html += '<tr>'
        for key in nat:
            html += '<td>'+str(nat[key])+'</td>'
        html += '</tr>'
    html += '</table><br>'

    writeOutput(pattern, html)

    # Additionnal information 
    with open("./info/nat.html", "r") as f:
        content = f.read()
        content += '<br><br><hr style="text-align:left;margin-left:0">'
        writeOutput('<!-- INFO - NAT --!>', content)


def parseConfig(configFile):
    root = ET.parse(configFile).getroot()
    
    ##################################
    # Host general info
    ##################################
    version = root.find('version').text
    host_info = {
            'Version' : version, 
            'Hostname' : "", 
            'Domain' : "", 
            'Timeservers' : "", 
            'IPv6 allowed' : False, 
            'Timezone' : "", 
            'Language' : "" }

    for system in root.find('system'):
        if system.tag == 'hostname':
            host_info['Hostname'] = system.text
        if system.tag == 'domain':
            host_info['Domain'] = system.text
        if system.tag == 'timeservers':
            host_info['Timeservers'] = system.text
        if system.tag == 'ipv6allow':
            host_info['IPv6 allowed'] = True
        if system.tag == 'timezone':
            host_info['Timezone'] = system.text
        if system.tag == 'language':
            host_info['Language'] = system.text

    writeInfo(host_info, "<!-- HOST --!>", "", "host")
    print('   + Host info : DONE !')


    ##################################
    # Users
    ##################################
    allUsers = []
    for user in root.findall('system/user'):
        user_info = {
                'Name' : '', 
                'Description' : "", 
                'Bcrypt hash' : "", 
                'UID' : "", 
                'Privileges' : "", 
                'Default password' : ''
                }
        for userItem in list(user):
            if userItem.tag == 'name':
                user_info['Name'] = userItem.text
            if userItem.tag == 'descr':
                user_info['Description'] = userItem.text
            if userItem.tag == 'bcrypt-hash':
                user_info['Bcrypt hash'] = userItem.text
            if userItem.tag == 'uid':
                user_info['UID'] = userItem.text
            if userItem.tag == 'priv':
                user_info['Privileges'] = userItem.text
        if user_info['Bcrypt hash'] != "":
            userHash =  user_info['Bcrypt hash'].encode()
            if bcrypt.checkpw(b'pfsense', userHash):
                user_info['Default password'] = '<b>True</b>'
            else:
                user_info['Default password'] = False
        allUsers.append(user_info)
    writeUsers(allUsers, "<!-- USERS --!>")
           

    ##################################
    # Admin access
    ##################################
    adminInfo = { 
            'SSH enabled' : False,
            'Redirect HTTP to HTTPS' : True,
            'Protocol': 'http',
            'Web session timeout': '240 min'
        }
    sshInfo = {
            'Key only': False,
            'Port' : '22'
        }
    for webItem in root.find('system/webgui'):
        if webItem.tag == 'protocol':
            adminInfo['Protocol'] = webItem.text
        if webItem.tag == 'disablehttpredirect':
            adminInfo['Redirect HTTP to HTTPS'] = False
        if webItem.tag == 'session_timeout':
            adminInfo['Web session timeout'] = str(webItem.text)+' min'
    for item in root.find('system'):
        if item.tag == 'enablesshd':
            adminInfo['SSH enabled'] = item.text
    if root.find('system/ssh') is not None:
        for sshItem in root.find('system/ssh'):
            if sshItem.tag == "sshdkeyonly":
                sshInfo['Key only'] = sshItem.text
            if sshItem.tag == 'port':
                sshInfo['Port'] == sshItem.text
    
    # If admin flag is set, the additional info for admin section will be added
    if adminInfo['SSH enabled'] == False:
        writeInfo(adminInfo, '<!-- ADMIN ACCESS --!>', "General", "admin")
    else:
        writeInfo(adminInfo, '<!-- ADMIN ACCESS --!>', "General", "")
        writeInfo(sshInfo, '<!-- SSH --!>', "SSH", "admin")

    print('   + Admin access : DONE !')
    print('   + SSH access : DONE ! - TODO auth keys')


    ##################################
    # Aliases
    ##################################
    allAliases = []
    for alias in root.findall('aliases/alias'):
        aliasInfo = {
                'Name' : '', 
                "Address": "",
                'Description' : "", 
                'Type' : ""
                }
        for aliasItem in list(alias):
            if aliasItem.tag == 'name':
                aliasInfo['Name'] = aliasItem.text
            if aliasItem.tag == 'address':
                aliasInfo['Address'] = aliasItem.text
            if aliasItem.tag == 'descr':
                aliasInfo['Description'] = aliasItem.text
            if aliasItem.tag == 'type':
                aliasInfo['Type'] = aliasItem.text
        allAliases.append(aliasInfo)
    if len(allAliases) > 0:
        writeAliases(allAliases, "<!-- ALIASES --!>")


    ##################################
    # Gateways (newer configs)
    ##################################
    # Old configs : 
    #   - gateways defined in the inferfaces > ifname > gateway section
    # Newer configs : 
    #   - Gateways defined in a separate block (see here)
    allGw = []
    defaultgw4 = ""
    defaultgw6 = ""
    gwGroup = dict()
    gwInfo = dict()
    if root.find('gateways') is not None:
        for gw in root.find('gateways'):
            if gw.tag == 'defaultgw4':
                defaultgw4 = gw.text
            if gw.tag == 'defaultgw6':
                defaultgw6 = gw.text
            if gw.tag == 'gateway_item':
                gwInfo = {
                        "Interface" : '', 
                        "Address": "",
                        "Name" : "", 
                        "Default": False,
                        "Description" : ""
                    }
                for gwItem in list(gw):
                    if gwItem.tag == 'interface':
                        gwInfo['Interface'] = gwItem.text
                    if gwItem.tag == 'gateway':
                        gwInfo['Address'] = gwItem.text
                    if gwItem.tag == 'name':
                        gwInfo['Name'] = gwItem.text
                    if gwItem.tag == 'defaultgw':
                        gwInfo['Default'] = True
                    if gwItem.tag == 'descr':
                        gwInfo['Description'] = gwItem.text
                allGw.append(gwInfo)
            if gw.tag == 'gateway_group':
                gwGroup = {
                        "Name" : '', 
                        "Items": [],
                        "Trigger" : "", 
                        "Description" : ""
                    }
                for gwItem in list(gw):
                    if gwItem.tag == 'name':
                        gwGroup['Name'] = gwItem.text
                    if gwItem.tag == 'item':
                        gwGroup['Items'].append(gwItem.text)
                    if gwItem.tag == 'trigger':
                        gwGroup['Trigger'] = gwItem.text
                    if gwItem.tag == 'descr':
                        gwGroup['Description'] = gwItem.text

        if len(allGw) > 0 or defaultgw4 != "" or defaultgw6 != "" or len(gwGroup) > 0:
            writeGw(allGw, defaultgw4, defaultgw6, gwGroup, "<!-- GATEWAYS --!>")
        print('   + Gateways: DONE !')



    ##################################
    # Interfaces
    ##################################
    allInterfaces = []
    for interface in root.find('interfaces'):
        pfSenseName = interface.tag
        interfaceInfo = { 
                'PfSense name' : pfSenseName,
                'System name' : '',
                'Interface enabled' : False,
                'IPv4' : '',
                'Subnet v4' : '', 
                'IPv6' : '',
                'Subnet v6' : '', 
                'Gateway' : ''
            }
        for child in interface:
            if child.tag == 'if':
                interfaceInfo['System name'] = child.text
            if child.tag == 'enable':
                interfaceInfo['Interface enabled'] = True
            if child.tag == 'ipaddr':
                interfaceInfo['IPv4'] = child.text
            if child.tag == 'subnet':
                interfaceInfo['Subnet v4'] = child.text
            if child.tag == 'ipaddrv6':
                interfaceInfo['IPv6'] = child.text
            if child.tag == 'subnetv6':
                interfaceInfo['Subnet v6'] = child.text
            if child.tag == 'gateway':
                interfaceInfo['Gateway'] = child.text
        
        allInterfaces.append(interfaceInfo)
    
    writeInterfaces(allInterfaces, "<!-- INTERFACES --!>")


    ##################################
    # Static routes
    ##################################
    allRoutes = []
    for route in root.find('staticroutes'):
        routeInfo = { 
                'Interface' : '',
                'Network' : '',
                'Gateway' : '',
                'Description' : ''
            }
        for routeitem in list(route):
            if routeitem.tag == 'interface':
                routeInfo['Interface'] = routeitem.text
            if routeitem.tag == 'network':
                routeInfo['Network'] = routeitem.text
            if routeitem.tag == 'gateway':
                routeInfo['Gateway'] = routeitem.text
            if routeitem.tag == 'descr': 
                routeInfo['Description'] = routeitem.text
        allRoutes.append(routeInfo)

    if len(allRoutes) > 0:
        writeRoutes(allRoutes, "<!-- ROUTES --!>")


    ##################################
    # DHCP
    ##################################

    print('   - DHCP: TODO...')



    ##################################
    # Filtering rules
    ##################################
    # Create dictionaries: 1 list of filtering rules per interface
    # Idem for separators
    allRules = {}
    allSeparators = {}

    sepInfo = {}
    ruleInfo = {}

    ifaceList = list(root.find('interfaces'))

    # Always add openvpn interface (not explicitely defined in the conf file)
    for i in range(len(ifaceList)):
        allRules[ifaceList[i].tag] = []
        allSeparators[ifaceList[i].tag] = []
    allRules['openvpn'] = []
    allSeparators['openvpn'] = []

    for rule in root.find('filter'):
        if rule.tag == 'separator':
            for iface in list(rule):
                for sepitem in list(iface):
                    sepInfo = { 
                        'Row' : '', 
                        'Description' : '', 
                        'Color' : '', 
                        }
                    for ruleitem in list(sepitem):
                        if ruleitem.tag == 'row':
                            sepInfo['Row'] = str(ruleitem.text[2:])
                        if ruleitem.tag == 'text':
                            sepInfo['Description'] = ruleitem.text
                        if ruleitem.tag == 'color':
                            sepInfo['Color'] = ruleitem.text
                    allSeparators[iface.tag].append(sepInfo)

        if rule.tag == 'rule':
            ruleInfo = { 
                'Enabled' : True,
                'Type' : '',
                'Interface' : '',
                'Protocole' : '',
                'Source' : '',
                'Destination' : '',
                'NAT id': '',
                'Comment' : ''
                }
            for ruleitem in list(rule):
                if ruleitem.tag == 'disabled':
                    ruleInfo['Enabled'] = False
                if ruleitem.tag == 'type':
                    ruleInfo['Type'] = ruleitem.text
                if ruleitem.tag == 'interface':
                    ruleInfo['Interface'] = ruleitem.text
                if ruleitem.tag == 'protocol': 
                    ruleInfo['Protocole'] += ' '+ruleitem.text
                if ruleitem.tag == 'ipprotocol':
                    ruleInfo['Protocole'] += ' '+ruleitem.text
                if ruleitem.tag == 'icmptype':
                    ruleInfo['Protocole'] += ' '+ruleitem.text
                if ruleitem.tag == 'statetype':
                    ruleInfo['Protocole'] += ' '+ruleitem.text
                if ruleitem.tag == 'source':
                    source = list(ruleitem)
                    if source[0] is not None:
                        if source[0].tag == 'address' :
                            address = ruleitem.find("address").text
                            ruleInfo['Source'] = "ADDR <br>"+address
                        if source[0].tag == 'network' :
                            network = ruleitem.find("network").text
                            ruleInfo['Source'] = "NET <br>"+network
                        if source[0].tag == 'any' :
                            ruleInfo['Source'] = "ANY"
                    if len(source) > 1:
                        port = source[1]
                        if source[1].tag == 'port' :
                            sport = str(source[1].text)
                            ruleInfo['Source'] += "<br>"+sport
                if ruleitem.tag == 'destination':
                    destination = list(ruleitem)
                    if destination[0] is not None:
                        if destination[0].tag == 'address' :
                            address = ruleitem.find("address").text
                            ruleInfo['Destination'] = "ADDR <br>"+address
                        if destination[0].tag == 'network' :
                            network = ruleitem.find("network").text
                            ruleInfo['Destination'] = "NET <br>"+network
                        if destination[0].tag == 'any' :
                            ruleInfo['Destination'] = "ANY"
                    if len(destination) > 1:
                        port = destination[1]
                        if destination[1].tag == 'port' :
                            dport = str(destination[1].text)
                            ruleInfo['Destination'] += "<br>"+dport
                if ruleitem.tag == 'descr':
                    ruleInfo['Comment'] = ruleitem.text
                if ruleitem.tag == 'associated-rule-id':
                    ruleInfo['NAT id'] = ruleitem.text
            if ruleInfo['Type'] == '':
                # Rules with no action defined default to 'pass'
                # See ./src/etc/inc/filter.inc
                ruleInfo['Type'] = 'pass'
            
            # Add current rule to the list of the targeted interface
            allRules[ruleInfo['Interface']].append(ruleInfo)
    
    if len(allRules) > 0:
        writeRules(allRules, allSeparators, "<!-- RULES --!>")


    ##################################
    # NAT - Port forwarding
    ##################################
    # Two types of NAT categories : 
    #  - rule : port forwarding
    #  - one to one
    # They have attributes in common (source, destination, interface...)
    
    # Lists of rules (port forwarding or 1to1
    allNat = []
    allNatOneToOne = []
    for nat in root.find('nat'):
        natOneToOneInfo = { 
                'Interface' : '',
                'Source' : '',
                'Destination' : '',
                'External' : '',
                'Description' : ''
            }
        # Port forwarding
        natInfo = { 
                'Interface' : '',
                'Source' : '',
                'Destination' : '',
                'Protocol' : '',
                'Target' : '',
                'Local Port' : '',
                'NAT id': '',
                'Description' : ''
            }
        for natitem in list(nat):
            # onetoone subblock : one to one NAT rules
            # rule sublock : port forwarding rule
            if nat.tag == 'onetoone' or nat.tag == "rule":
                if natitem.tag == 'interface': 
                    natInfo['Interface'] = natitem.text
                    natOneToOneInfo['Interface'] = natitem.text
                if natitem.tag == 'descr':
                    natInfo['Description'] += str(natitem.text)
                    natOneToOneInfo['Description'] = str(natitem.text)
                if natitem.tag == 'source':
                    source = list(natitem)
                    if source[0] is not None:
                        if source[0].tag == 'address' :
                            address = natitem.find("address").text
                            natInfo['Source'] = "ADDR <br>"+address
                            natOneToOneInfo['Source'] = "ADDR <br>"+address
                        if source[0].tag == 'network' :
                            network = natitem.find("network").text
                            natInfo['Source'] = "NET <br>"+network
                            natOneToOneInfo['Source'] = "NET <br>"+network
                        if source[0].tag == 'any' :
                            natInfo['Source'] = "ANY"
                            natOneToOneInfo['Source'] = "ANY"
                    if len(source) > 1:
                        port = source[1]
                        if source[1].tag == 'port' :
                            sport = source[1].text
                            natInfo['Source'] += "<br>"+sport
                            natOneToOneInfo['Source'] += "<br>"+sport
                if natitem.tag == 'destination':
                    destination = list(natitem)
                    if destination[0] is not None:
                        if destination[0].tag == 'address' :
                            address = natitem.find("address").text
                            natInfo['Destination'] = "ADDR <br>"+address
                            natOneToOneInfo['Destination'] = "ADDR <br>"+address
                        if destination[0].tag == 'network' :
                            network = natitem.find("network").text
                            natInfo['Destination'] = "NET <br>"+network
                            natOneToOneInfo['Destination'] = "NET <br>"+network
                        if destination[0].tag == 'any' :
                            natInfo['Destination'] = "ANY"
                            natOneToOneInfo['Destination'] = "ANY"
                    if len(destination) > 1:
                        port = destination[1]
                        if destination[1].tag == 'port' :
                            dport = destination[1].text
                            natInfo['Destination'] += "<br>"+dport
                            natOneToOneInfo['Destination'] += "<br>"+dport

            if nat.tag == 'onetoone':
                if natitem.tag == 'external': 
                    natOneToOneInfo['External'] = natitem.text
            
            if nat.tag == 'rule':
                if natitem.tag == 'protocol':
                    natInfo['Protocol'] = natitem.text
                if natitem.tag == 'target':
                    natInfo['Target'] = natitem.text
                if natitem.tag == 'local-port':
                    natInfo['Local Port'] = natitem.text
                if natitem.tag == 'associated-rule-id':
                    natInfo['NAT id'] = natitem.text

        if natInfo['Interface'] != '' and natInfo['Local Port'] != '':
            allNat.append(natInfo)

        if natOneToOneInfo['Interface'] != '' and natOneToOneInfo['External'] != '':
            allNatOneToOne.append(natOneToOneInfo)
    
    if len(allNat) > 0 and len(allNatOneToOne) == 0:
        writeNat(allNat, "<!-- NAT RULES --!>", "Port forwarding", True)
    if len(allNat) > 0 and len(allNatOneToOne) > 0:
        writeNat(allNat, "<!-- NAT RULES --!>", "Port forwarding", True)
        writeNat(allNatOneToOne, "<!-- NAT ONE TO ONE --!>", "One to One", False)
    if len(allNat) == 0 and len(allNatOneToOne) > 0:
        writeNat(allNatOneToOne, "<!-- NAT ONE TO ONE --!>", "One to One", True)
    print('   + NAT: DONE !')


    ##################################
    # Syslog
    ##################################
    localLoggingInfo = { 
            'Enabled' : True
        }
    remoteLoggingInfo = { 
            'Enabled' : False,
            'Remote server 1' : "",
            'Remote server 2' : "",
            'Remote server 3' : "",
            'Events': ''
        }
    remoteLoggingOptions = ['vnp', 'dpinger', 'resolver', 'dhcp', "hostapd", 
            "filter", "portalauth", "system"]
    for logItem in root.find('syslog'):
        if logItem.tag == 'enable':
            remoteLoggingInfo['Enabled'] = True
        if logItem.tag == 'remoteserver':
            remoteLoggingInfo['Remote server 1'] = logItem.text
        if logItem.tag == 'remoteserver2':
            remoteLoggingInfo['Remote server 2'] = logItem.text
        if logItem.tag == 'remoteserver3':
            remoteLoggingInfo['Remote server 3'] = logItem.text
        if logItem.tag == 'logall':
            remoteLoggingInfo['Events'] += " ALL"
        if logItem.tag in [] :
            remoteLoggingInfo['Events'] += " "+logItem.tag
        if logItem.tag == 'disablelocallogging':
            localLoggingInfo['Enabled'] == False
    
    # Fix : if remote logging is enabled, local logging is disabled.
    # No !
    #if remoteLoggingInfo["Enabled"] == True:
    #    localLoggingInfo["Enabled"] = False

    writeInfo(localLoggingInfo, '<!-- LOCAL LOGGING --!>', "Local logging", "syslog")
    
    if remoteLoggingInfo['Enabled'] == True:
        writeInfo(remoteLoggingInfo, '<!-- REMOTE LOGGING --!>', "Remote logging", "syslog")

    print('   + Syslog : DONE !')


    ##################################
    # SNMP
    ##################################
    snmpInfo = { 
            'Enabled' : False
        }
    for snmpItem in root.find('snmpd'):
        if snmpItem.tag == 'enable':
            loggingInfo['Enabled'] = True

    writeInfo(snmpInfo, '<!-- SNMP --!>', "", "snmp")
    print('   ~ SNMP : PARTIALLY DONE !')


    ##################################
    # Server and CA certificates
    ##################################
    allCerts = []
    allCa = []
    listCerts = root.findall('cert')
    listCa = root.findall('ca')
    for cert in listCerts+listCa:
        certInfo = { 
                'Id' : '',
                'Description' : '',
                'Certificate' : '',
                'Private' : ''
            }
        for certItem in list(cert):
            if certItem.tag == 'refid':
                certInfo['Id'] = certItem.text
            if certItem.tag == 'descr':
                certInfo['Description'] = certItem.text
            if certItem.tag == 'crt':
                certInfo['Certificate'] = 'CERT (b64): '+certItem.text
            if certItem.tag == 'prv':
                certInfo['Private'] = 'PRIV (b64): '+certItem.text
        if cert.tag == 'cert':
            allCerts.append(certInfo)
        if cert.tag == 'ca':
            allCa.append(certInfo)
    if len(allCerts) > 0 or len(allCa) > 0:
        writeCert(allCerts, allCa, '<!-- CERT --!>')
    print('   + Certificates : Done !')


    ##################################
    # OpenVPN 
    ##################################
    allOvpnServ = []
    for ovpnServ in root.findall('openvpn/openvpn-server'):
        cryptoInfo = {
                'Id' : '',
                'Compression' : '',
                'Certref' : '',
                'Crypto' : '',
                'Digest' : '',
                'TLS type': '',
                'NCP enabled' : False,
                'NCP ciphers' : ''
            }
        ovpnServInfo = { 
                'Id' : '',
                'Mode' : '',
                'Protocol' : '',
                'Device Mode' : '',
                'Interface' : '',
                'Local port' : '',
                'Local net' : '',
                'Remote net' : '',
                'Tunnel net' : '',
                'Description' : ''
            }
        for ovpnServItem in list(ovpnServ):
            if ovpnServItem.tag == 'vpnid':
                ovpnServInfo['Id'] = ovpnServItem.text
                cryptoInfo['Id'] = ovpnServItem.text
            if ovpnServItem.tag == 'mode':
                ovpnServInfo['Mode'] = ovpnServItem.text
            if ovpnServItem.tag == 'protocol':
                ovpnServInfo['Protocol'] = ovpnServItem.text
            if ovpnServItem.tag == 'dev_mode':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Device Mode'] = ovpnServItem.text
            if ovpnServItem.tag == 'interface':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Interface'] = ovpnServItem.text
            if ovpnServItem.tag == 'local_port':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Local port'] = ovpnServItem.text
            if ovpnServItem.tag == 'local_network':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Local net'] = ' '+ovpnServItem.text
            if ovpnServItem.tag == 'local_networkv6':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Local net'] = ' '+ovpnServItem.text
            if ovpnServItem.tag == 'tunnel_network':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Tunnel net'] = ' '+ovpnServItem.text
            if ovpnServItem.tag == 'tunnel_networkv6':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Tunnel net'] = ' '+ovpnServItem.text
            if ovpnServItem.tag == 'remote_network':
                    ovpnServInfo['Remote net'] = ' '+ovpnServItem.text
            if ovpnServItem.tag == 'remote_networkv6':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Remote net'] = ' '+ovpnServItem.text
            if ovpnServItem.tag == 'description':
                if ovpnServItem.text is not None:
                    ovpnServInfo['Description'] = ovpnServItem.text
            
            if ovpnServItem.tag == 'certref':
                if ovpnServItem.text is not None:
                    cryptoInfo['Certref'] = ovpnServItem.text
            if ovpnServItem.tag == 'crypto':
                if ovpnServItem.text is not None:
                    cryptoInfo['Crypto'] = ovpnServItem.text
            if ovpnServItem.tag == 'digest':
                if ovpnServItem.text is not None:
                    cryptoInfo['Digest'] = ovpnServItem.text
            if ovpnServItem.tag == 'tls_type':
                if ovpnServItem.text is not None:
                    cryptoInfo['TLS type'] = ovpnServItem.text
            if ovpnServItem.tag == 'ncp_enable':
                if ovpnServItem.text is not None:
                    cryptoInfo['NCP enabled'] = ovpnServItem.text
            if ovpnServItem.tag == 'ncp-ciphers':
                if ovpnServItem.text is not None:
                    cryptoInfo['NCP ciphers'] = ovpnServItem.text
            if ovpnServItem.tag == 'compression':
                if ovpnServItem.text is not None:
                    cryptoInfo['Compression'] = ovpnServItem.text

        allOvpnServ.append([ovpnServInfo, cryptoInfo])
    if len(allOvpnServ) > 0:
        writeOvpn(allOvpnServ, '<!-- OpenVPN SERVERS --!>')
    print('   + OpenVPN: Done!')


    ##################################
    # IPsec
    ##################################

    #print('   - IPsec: TODO...')


    ##################################
    # HA - VIP
    ##################################

    #print('   - HA / VIP: TODO...')



def main():

    parser = argparse.ArgumentParser(description="pfSense configuration parser")
    parser.add_argument("-i", dest="filename", required=True,
                    help="Exported configuration file", metavar="FILE")

    args = parser.parse_args()

    if not os.path.exists(args.filename):
        parser.error("The file %s does not exist!" % args.filename)

    if not os.path.exists('template.html'):
        sys.exit("The template file (template.html) does not exist!")
    
    shutil.copyfile('template.html', 'output.html')
    configFile = args.filename

    # Parse config file and produce output.html
    print('> Beginning parsing...')
    parseConfig(configFile)
    
    # Write date and time
    now = datetime.now()
    writeOutput("-----TIME AND DATE-----", now.strftime("%a, %d %b %Y %H:%M:%S"))

main()
